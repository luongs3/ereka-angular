import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../environments/environment';
import { User } from './user/user';

export let httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private url = environment.API_LOGIN;
  private api = environment.API_URL;
  public authUser: User;

  constructor(private httpClient: HttpClient) { }

  postLogin(infor: object) {
    this.httpClient.post(this.url, infor, httpOptions)
      .subscribe(response => {
        this.setToken(response.token);
        this.fetchMe().subscribe(res => {
          if ('result' in res) {
            this.authUser = res.result.data;
          }
        });
      });
  }

  setToken(token) {
    this.setHeader(token);
    localStorage.setItem('access_token', token);
  }

  rehydrateToken(): string {
    const token = localStorage.getItem('access_token');
    if (token) {
      this.setHeader(token);
    }

    return token;
  }

  setHeader(token) {
    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
        'Access-Control-Allow-Origin': '*',
      })
    };
  }

  fetchMe(): Observable<Object> {
    return this.httpClient.get(`${this.api}/api/me`, httpOptions);
  }
}
