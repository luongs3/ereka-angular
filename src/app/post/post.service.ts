import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import { httpOptions } from '../authentication.service';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private api = environment.API_URL;

  constructor(private httpClient: HttpClient) { }

  fetchPosts() {
    console.log('httpOptions', httpOptions);
    return this.httpClient.get(`${this.api}/content/posts/home`, httpOptions);
  }
}
