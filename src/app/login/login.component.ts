import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {AuthenticationService} from '../authentication.service';
import {UserService} from '../user/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  constructor(
    private location: Location,
    private authenService: AuthenticationService,
    private userService: UserService,
    ) { }

  ngOnInit() {
  }

  back() {
    this.location.back();
  }

  login(username, password) {
    this.authenService.postLogin({username, password});
  }
}
