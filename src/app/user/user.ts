export class User {
  id: string;
  roles: string;
  full_name: string;
  username: string;
  about: string;
  location: string;
  score: number;
  email: string;
  cover_url: string;
  avatar_url: string;
  title: string;
  num_coin: number;
  anonymous: boolean;
}
