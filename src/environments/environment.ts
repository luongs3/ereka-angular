// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  API_URL: 'http://10.240.152.225:9090',
  API_LOGIN: 'http://10.240.152.164:8887/api/authenticate',
  URL_AUTH_SOCIAL: 'http://10.240.152.164:8887',
  URL_UPLOAD_FILE: 'http://10.240.152.164:8887/api/upload',
  URL_SUGGEST: 'http://10.240.152.225:9795',
  WEB_CLIENT_ID: '903369020830-5jtrs40rfb47057sihgadnsq3qd33u6f.apps.googleusercontent.com',
  FB_APP_ID: '304141440095066',
  URL_LINK_PREVIEW: 'https://parser.api.ereka.vn/previews',
  API_CHAT: 'http://10.240.152.228:5001',
  WEBSOCKET_CHAT: 'ws://10.240.152.228:8000',
  API_UPLOAD_FILE: 'http://10.240.152.164:8887/api/upload/file',
  DEBUGGING_SCREEN: 'Exploration',
  DOMAIN_SHARE: 'http://dev.ereka.vn/',
  SENTRY_DNS: 'https://b8c3ac63797345b1860f7f6e05d5081d@sentry.io/1354644',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
