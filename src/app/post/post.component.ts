import { Component, OnInit } from '@angular/core';
import {Post} from './post';
import {PostService} from './post.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnInit {
  posts: Post[];
  page: 0;
  loading: false;
  total: 0;
  loadMoreAble: true;

  constructor(private postService: PostService) { }

  ngOnInit() {
    this.postService.fetchPosts()
      .subscribe(res => {
        if (res && res.result) {
          const { items, page, total, load_more_able: loadMoreAble } = res.result;
          this.posts = items;
          this.page = page;
          this.total = total;
          this.loadMoreAble = loadMoreAble;
        }
      });
  }

}
