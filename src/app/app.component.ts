import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import {AuthenticationService} from './authentication.service';
import { Router } from '@angular/router';
import {UserService} from './user/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit{
  title = 'ereka';

  constructor(
    private authenService: AuthenticationService,
    private router: Router
  ) { }

  ngOnInit() {
    const token = this.authenService.rehydrateToken();
    if (token) {
      this.router.navigateByUrl('/post');
      return;
    }

    this.router.navigateByUrl('/login');
  }

}
