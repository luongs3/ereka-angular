export class Post {
  id: string;
  title: string;
  content: string;
  short_description: string;
  owner_id: string;
  owner: object;
  post_type: string;
  topics: object;
  vote_point: number;
  num_up_vote: number;
  num_down_vote: number;
  vote_status: number;
  cover_url: string;
  comments: object;
}
